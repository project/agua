<?php

/**
 * @file preprocess-node-content.inc
 *
 * Create alternate template file name for the specific node types.
 */
$vars['template_files'][] = 'node-content-'. str_replace('_', '-', $vars['node']->type);

/**
 * By default, set the display to be the regular content.
 */
 $vars['display'] = $vars['content'];