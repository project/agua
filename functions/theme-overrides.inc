<?php

/**
 * @file theme-overrides.inc
 *
 * Set of functions to override default ones.
 *
 */
 
/**
 * @see theme_breacrumb(), agua_theme()
 * http://api.drupal.org/api/function/theme_breadcrumb/6
 *
 * Added: changed breadcrumb separator to image provided by theme
 * Removed: Class attribute: breadcrumb
 */
function agua_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    // Use separator defined by theme settings
    $separator = '<img src="/'. path_to_theme() .'/images/arrow.gif" />';
    return '<div id="breadcrumbs">'. implode(' '. $separator .' ', $breadcrumb) .'</div>';
  }
}

 /**
 * @see theme_menu_local_tasks(), agua_theme()
 * http://api.drupal.org/api/function/theme_menu_local_tasks/6
 *
 * Added: A wrapper <div>'s with class attributes: a helper class
 * indicating what's inside, and id attributes.
 * borrowed from sky http://drupal.org/project/sky
 */
function agua_menu_local_tasks() {
  $primary = menu_primary_local_tasks();
  $secondary = menu_secondary_local_tasks();

  // What's inside
  if ($primary && $secondary) {
    $helper_class = ' primary-and-secondary';
  }
  else {
    $helper_class = ' primary-only';
  }

  if ($primary || $secondary) {
    $output = '<div class="tab-wrapper'. $helper_class .'">';

    if ($primary) {
      $output .= "\n".'<div id="tabs-primary" class="tabs primary">'."\n".
      '  <ul>'."\n". $primary .'</ul>'."\n".
      '</div>';
    }
    if ($secondary) {
      $output .= "\n".'<div id="tabs-secondary" class="tabs secondary">'."\n" .
      '  <ul>'."\n". $secondary .'</ul>'."\n" .
      '</div>';
    }
    $output .= '</div>';
  }

  return $output;
}

/**
 * Override, use better icons, source: http://drupal.org/node/102743#comment-664157
 *
 * Format the icon for each individual topic.
 *
 * @ingroup themeable
 */
function agua_forum_icon($new_posts, $num_posts = 0, $comment_mode = 0, $sticky = 0) {
  // because we are using a theme() instead of copying the forum-icon.tpl.php into the theme
  // we need to add in the logic that is in preprocess_forum_icon() since this isn't available
  if ($num_posts > variable_get('forum_hot_topic', 15)) {
    $icon = $new_posts ? 'hot-new' : 'hot';
  }
  else {
    $icon = $new_posts ? 'new' : 'default';
  }

  if ($comment_mode == COMMENT_NODE_READ_ONLY || $comment_mode == COMMENT_NODE_DISABLED) {
    $icon = 'closed';
  }

  if ($sticky == 1) {
    $icon = 'sticky';
  }

  $output = theme('image', path_to_theme() . "/images/forum-$icon.png");

  if ($new_posts) {
    $output = "<a name=\"new\">$output</a>";
  }

  return $output;
}
