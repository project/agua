<?php

/**
 * @file template.php
 *
 * Main template file for this theme.
 */

/**
 * Implementation of hook_theme().
 *
 * @return
 */
function agua_theme() {
  return array(
    'breadcrumb' => array(
      'arguments' => array('breadcrumb' => array()),
      'file' => 'functions/theme-overrides.inc',
    ),
    'menu_local_tasks' => array(
      'arguments' => NULL,
      'file' => 'functions/theme-overrides.inc',
    ),
    'forum_icon' => array(
      'arguments' => array('new_posts' => NULL, 'num_posts' => NULL, 'comment_mode' => NULL, 'sticky' => NULL),
      'file' => 'functions/theme-overrides.inc',
    ),
    'id_safe' => array(
      'arguments' => array('string'),
      'file' => 'functions/theme-custom.inc',
    ),
    'conditional_stylesheets' => array(
      'file' => 'functions/theme-custom.inc',
    ),
    'render_attributes' => array(
      'arguments' => array('attributes'),
      'file' => 'functions/theme-custom.inc',
    ),
  );
}

/**
 * Add Superfish JS file if it exists
 * 
 */
$superfish = drupal_get_path('theme', 'agua') .'/js/superfish.js';
if (file_exists ($superfish)) {
  drupal_add_js($superfish);
}

/**
 * Implementation of hook_preprocess()
 * 
 * This function checks to see if a hook has a preprocess file associated with 
 * it, and if so, loads it.
 * 
 * @param $vars
 * @param $hook
 * @return Array
 */
function agua_preprocess(&$vars, $hook) {
  
  if (is_file(drupal_get_path('theme', 'agua') .'/preprocess/preprocess-'. str_replace('_', '-', $hook) .'.inc')) {
    include 'preprocess/preprocess-'. str_replace('_', '-', $hook) .'.inc';
  }
}

/**
 * Implementation of hook_preprocess.
 * 
 * Since it is difficult to properly determine the path of a theme and subthemes 
 * when the system is offline, the specific preprocess funtion for the 
 * maintenance_page hook is called directly.
 * 
 * @param $vars
 * @return Array
 */
function agua_preprocess_maintenance_page(&$vars) {
  $vars['body_attributes']['id'] = 'maintenance-page';
  
  $vars['body_attributes']['class'][] = $vars['body_classes'];
  
  $vars['attributes'] = theme('render_attributes', $vars['body_attributes']);
}
